# Code challenge #

### Dependencies ###

* Docker (https://docs.docker.com/engine/installation/)
* Docker-compose 1.8 (https://docs.docker.com/compose/install/)

### Setup ###

* Clone the project, if you use macOS, make sure that you clone the repository under `/Users` directory because there is some restrictions of docker in macOS
* Build containers, there are two containers (app and hadoop). App container contains main app whereas Hadoop container simulates a standalone spark cluster with hadoop HDFS.
`docker-compose build`
* Open `docker-compose.yml` to adjust hadoop standalone cluster. For example, you want to have a cluster with 2 workers (2 cores and 2 GB)

```
#!python
SPARK_WORKER_CORES: 2
SPARK_WORKER_MEMORY: 2G
SPARK_WORKER_INSTANCES: 2

```


* Start the hadoop container
`docker-compose up hadoop`
* You can verify by running `docker ps`, access to HDFS page container_ip:50070 and spark master page container_ip:50070. Depending on the system, ip could be change, if you use ubuntu, that's localhost.

* Stop hadoop container by running
`docker-compose stop`

### Configuration ###
Copy `luigi.cfg.example` to new file `luigi.cfg` in the same directory

Give your `[S3]` account credentials and a `bucket` which this account can access, for example `lenguyenhaohiep` or 'you_bucket/your_sub_bucket/test'. This is where you find the result

Modify `[spark]` parameters depending on your system, just `total-executor-cores` and `executor-memory`. Leave the others by default, they are well configured.
```
#!python

[s3]
aws_access_key_id = <Your Key ID>
aws_secret_access_key = <Your Access Key>

[test_bucket]
bucket = <bucket which the AWS account can access> # lenguyenhaohiep

[spark]
spark-submit = /usr/local/spark/bin/spark-submit
master = spark://hadoop:7077
packages = org.apache.hadoop:hadoop-aws:2.6.0
executor-memory = 4G
total-executor-cores = 4
```

### How to run app ###
To run the pipeline, execute the following commands:

* To run the pipeline of current day and last hour

`docker-compose run app luigi --module run ExecuteJob --local-scheduler`

* To run the pipeline of specific day and hour

`docker-compose run app luigi --module run ExecuteJob --date 2016-01-01 --hour 1 --local-scheduler`

* To run the pipeline of specific day and a range of hours

`docker-compose run app luigi --module run ExecuteJob --date 2016-01-01 --hour [1,10] --local-scheduler --workers 2`

* To run the pipeline of a list of day and a range of hours

`docker-compose run app luigi --module run ExecuteJob --date 2016-01-01,2016-01-02 --hour [1,10] --local-scheduler --workers 5`

The result could be found in S3 and inside `app/data/result`

### How to run test ###

* To test tasks, execute 

`docker-compose run app python /app/test/test_tasks.py`

* To test the pipeline, execute

`docker-compose run app python /app/test/test_pipeline.py`