import luigi
import datetime


class TaskParameters:
    """
    Base Task to be derived by tasks in the same pipeline
    Properties:
        date    format YYYY-MM-DD
        hour    format 0-23
        force   Execute the task although it has been already executed
        dependency requisites (tasks) of the task
    """

    date = luigi.Parameter(default=datetime.datetime.now().strftime("%Y-%m-%d"))
    hour = luigi.Parameter(default=str(datetime.datetime.now().hour - 1))
    force = luigi.BoolParameter(default=False, significant=False)
    clear = luigi.BoolParameter(default=False, significant=False)
    s3 = luigi.BoolParameter(default=False, significant=False)
    dependency = None
