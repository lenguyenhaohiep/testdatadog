from tasks.taskbase import TaskBase
import luigi
import urllib.request

from  urllib.error import HTTPError
import time

class DownloadFile(TaskBase):
    """
    Download a file
    """
    local_path = luigi.Parameter()
    file_url = luigi.Parameter()

    def output(self):
        return luigi.LocalTarget(self.local_path)

    def run(self):
        try:
            request = urllib.request.urlretrieve(self.file_url, self.output().path)
        except Exception as e:
            if e.code == 503:
                # Redownload in case of Service temporarily unavailable
                time.sleep(1)
                self.run()
            else:
                # In case of download error, file is not complete
                if self.output().exists():
                    self.output().remove()

                raise Exception ("Download Error")

