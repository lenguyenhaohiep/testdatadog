from tasks.taskbase import TaskBase
from settings.task_settings import TaskSettings

import luigi
from luigi.contrib.webhdfs import WebHdfsTarget


class SaveToHDFS(TaskBase):
    """
    Save a file to HDFS
    """
    hdfs_path = luigi.Parameter()

    def output(self):
        return WebHdfsTarget(self.hdfs_path, TaskSettings.get_hdfs_client())

    def run(self):
        TaskSettings.get_hdfs_client().upload(self.hdfs_path, self.input().path, overwrite=True)
        # remove local file
        if self.clear:
            self.input().remove()