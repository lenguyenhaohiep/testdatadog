from tasks.taskbase import TaskBase
from luigi.s3 import S3Target
from settings.task_settings import TaskSettings

import luigi


class SaveToS3(TaskBase):
    """
    Save to S3
    """
    s3_url = luigi.Parameter()

    def output(self):
        return S3Target(self.s3_url, TaskSettings.get_s3client())

    def run(self):
        TaskSettings.get_s3client().put(self.input().path, self.s3_url)
        if self.clear:
            self.input().remove()
