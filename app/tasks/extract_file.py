from tasks.taskbase import TaskBase
import luigi
import gzip


class ExtractFile(TaskBase):
    """
    Extract the data from the gz file and add header to the file for the later use
    """
    headers = ["domain_code", "page_title", "count_views", "total_response_size"]
    local_path = luigi.Parameter()

    def output(self):
        return luigi.LocalTarget(self.local_path)

    def run(self):
        try:
            # Extract gzip file
            f = gzip.open(self.input().path, 'rb')
            file_content = f.read()
            f.close()

            space = " "

            # Add header and write to new file
            with open(self.output().path, "wb") as out:
                out.write(space.join(self.headers).encode())
                out.write("\n".encode())
                out.write(file_content)

            if self.clear:
                # Remove the gzip file
                self.input().remove()
        except:
            raise Exception(self.input().path)
            if self.output().exists():
                self.output().remove()
