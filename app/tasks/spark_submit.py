import luigi
from luigi.contrib.spark import SparkSubmitTask
from tasks.parameters import TaskParameters
from settings.task_settings import TaskSettings
from luigi.s3 import S3Target, S3Client
import os
import re
import shutil
from tasks.taskbase import TaskBase
import datetime


class SparkSubmit(SparkSubmitTask, TaskParameters):
    """
    Submit a spark job
    """

    app = luigi.Parameter()
    local_path = luigi.Parameter()
    temp_dir = "/tmp/"

    def requires(self):
        return self.dependency

    def app_options(self):
        if self.s3:
            # Use S3
            return [i.path for i in self.input()] + ["s3n://" + TaskSettings.parser.get("test_bucket", "bucket") + self.temp_dir + self.task_id]
        else:
            # Use HDFS
            host = TaskSettings.get_hdfs_url()
            hdfs_client = TaskSettings.get_hdfs_client()

            if hdfs_client.exists(self.temp_dir + self.task_id):
                hdfs_client.remove(self.temp_dir + self.task_id, skip_trash=True)

            return [host + i.path for i in self.input()] + [host + self.temp_dir + self.task_id]

    def spark_command(self):
        # tmp dependency dir
        command = super().spark_command()
        command += ["--conf", "spark.jars.ivy=/tmp/.ivy2/" + self.task_id]
        return command

    @property
    def packages(self):
        if self.s3:
            return super().packages
        return None

    def run(self):
        super().run()

        if self.s3:
            s3client = TaskSettings.get_s3client()
            s3_tmp = "s3n://" + TaskSettings.parser.get("test_bucket", "bucket") + self.temp_dir + self.task_id
            if s3client.exists(s3_tmp):
                paths = s3client.listdir(s3_tmp)
                with open(self.output().path, "wb") as fout:
                    for path in paths:
                        if re.search("part-r-\S+.csv$", path) is not None:
                            fout.write(s3client.get_as_string(path))
                s3client.remove(s3_tmp)

        else:
            hdfs_client = TaskSettings.get_hdfs_client()

            # Temp dir where output is written, multiple files part-xxxxx
            dir_path = self.temp_dir + self.task_id

            if hdfs_client.exists(dir_path):
                for root, dirs, files in hdfs_client.walk(dir_path):
                    with open(self.output().path, "wb") as fout:
                        for file in files:
                            if file != "_SUCCESS":
                                if re.search("^part-r-\S+.csv$", file) is not None:
                                    with hdfs_client.client.read(dir_path + "/" + file) as reader:
                                        fout.write(reader.read())

                hdfs_client.remove(dir_path, skip_trash=True)

        # remove the temp dependencies folder
        shutil.rmtree("/root/.ivy2/" + self.task_id, ignore_errors=True)

    def output(self):
        return luigi.LocalTarget(self.local_path)

    def exec_force(self):
        if self.force is True:
            if self.output().exists():
                self.output().remove()
