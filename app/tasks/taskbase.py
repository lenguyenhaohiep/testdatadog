import luigi

from tasks.parameters import TaskParameters
from luigi.s3 import S3Client, S3Target
from settings.task_settings import TaskSettings
from luigi.contrib.webhdfs import WebHdfsTarget

class TaskBase(TaskParameters, luigi.Task):
    """
    The task base in the pipeline
    """

    def exec_force(self):
        if self.force is True:
            if self.output().exists():
                print("remove %s" % (self.output().path))
                if isinstance(self.output(), WebHdfsTarget):
                    TaskSettings.get_hdfs_client().remove(self.output().path, skip_trash=True)
                else:
                    self.output().remove()


    def requires(self):
        return self.dependency
