from tasks.parameters import TaskParameters

from settings.pipeline_builder import PipelineBuilder
import luigi, datetime


class ExecuteJob(luigi.WrapperTask, TaskParameters):
    pipeline_builder = None

    def __init__(self, *args, **kwargs):
        super(ExecuteJob, self).__init__(*args, **kwargs)
        self.pipeline_builder = PipelineBuilder(**kwargs)

    def requires(self):
        return self.pipeline_builder.get_last_tasks()
