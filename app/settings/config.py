from configparser import ConfigParser


class Config:
    """
    Class to read configuration file luigi.cfg
    """
    def __init__(self):
        self.parser = ConfigParser()
        self.parser.read("luigi.cfg")

    def get(self, section, option):
        return self.parser.get(section=section, option=option)
