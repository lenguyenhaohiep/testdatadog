import luigi

from tasks.download_file import DownloadFile
from tasks.extract_file import ExtractFile
from tasks.save_to_s3 import SaveToS3
from tasks.save_to_hdfs import SaveToHDFS
from tasks.spark_submit import SparkSubmit
from settings.task_settings import TaskSettings

from urllib.request import Request, urlopen
from urllib.error import HTTPError, URLError

import copy
import re
import datetime


class PipelineBuilder:
    """
    Build the pipeline
    """

    last_tasks = None  # Last tasks of pipeline which will start first
    parameters = None  # Parameters

    def __init__(self, **kwargs):
        self.parameters = kwargs
        if "date" not in kwargs:
            self.parameters["date"] = datetime.datetime.now().strftime("%Y-%m-%d")
        if "hour" not in kwargs:
            self.parameters["hour"] = str(datetime.datetime.now().hour - 1)
        self.build()

    def create_task(self, class_name, parameters, requires=None, **options):
        """
        Create a task in pipeline
        :param class_name:
        :param parameters:
        :param requires: Dependencies of the task
        :param options: Another parameters which are not basic paramters
        :return:
        """
        params = copy.copy(parameters)

        for opt in options:
            params[opt] = options[opt]

        task = class_name(**params)
        task.dependency = requires

        # Trigger force mode, delete the output
        if "force" in self.parameters:
            if hasattr(task, "exec_force"):
                task.exec_force()

        return task

    def _check_url_valid(self, url):
        """
        Check a given url is valid
        :param url:
        :return:
        """
        try:
            urlopen(Request(url))
        except HTTPError as e:
            print(e.code, url)
            return False
        except URLError as e:
            print(e.reason, url)
            return False
        else:
            return True

    def _get_hours(self):
        """
        Get hours from input
        there are 3 options
        --hour 1
        --hour [1,23] --> range
        --hour 1,2,3,4,5 --> list
        :return:
        """
        re_hour_range = "^\[(\d{1,2}),(\d{1,2})\]$"
        re_hour_list = "(\d+),*"

        hour = self.parameters["hour"]

        if re.search(re_hour_list, hour) is not None and "[" not in hour and "]" not in hour:
            res = [str(h) for h in re.findall(re_hour_list, hour)]
            return res

        if re.search(re_hour_range, hour) is not None:
            start, end = re.findall(re_hour_range, hour)[0]

            start = int(start)
            end = int(end)

            if start > end:
                raise Exception("Incorrect hour interval, start hour > end hour")

            if start <= end:
                res = [str(i) for i in range(start, end + 1)]
            return res

        raise Exception("Error 'hour' parsing")

        return res

    def _get_dates(self):
        """
        Process date parameter
        --date 2016-01-01
        --date [date1,date2]
        --date date1,date2,date3,...
        :return:
        """
        re_day_range = "^\[(\d{4}-\d{2}-\d{2}),(\d{4}-\d{2}-\d{2})\]$"
        re_day_list = "(\d{4}-\d{2}-\d{2}),*"

        day = self.parameters["date"]

        if re.search(re_day_list, day) is not None and "[" not in day and "]" not in day:
            res = [str(h) for h in re.findall(re_day_list, day)]
            return res

        if re.search(re_day_range, day) is not None:
            start, end = re.findall(re_day_range, day)[0]
            start = datetime.datetime.strptime(start, "%Y-%m-%d")
            end = datetime.datetime.strptime(end, "%Y-%m-%d")
            res = []

            if start > end:
                raise Exception("Incorrect date interval, start date > end date")

            while start <= end:
                res.append(start.strftime("%Y-%m-%d"))
                start += datetime.timedelta(days=1)

            return res

        raise Exception("Error 'date' parsing")

        return res

    def _get_parameters(self):
        """
        Find all combinations of parameters
        :return:
        """
        params = []
        dates = self._get_dates()
        hours = self._get_hours()

        for d in dates:
            for h in hours:
                params.append({
                    'date': d,
                    'hour': h,
                    'force': True if "force" in self.parameters else False,
                    'clear': True if "clear" in self.parameters else False,
                    's3': True if "s3" in self.parameters else False
                })

        return params

    def build(self):
        """
        Build the pipeline
        :return:
        """
        self.last_tasks = []

        for param in self._get_parameters():
            task_setting = TaskSettings(param["date"], param["hour"])
            check_url = self._check_url_valid(task_setting.get_file_url())

            if check_url:
                task1 = self.create_task(class_name=DownloadFile,
                                         parameters=param,
                                         file_url=task_setting.get_file_url(),
                                         local_path=task_setting.get_compressed_path())

                task2 = self.create_task(class_name=ExtractFile,
                                         requires=task1,
                                         parameters=param,
                                         local_path=task_setting.get_uncompressed_path())
                if param["s3"]:
                    task3 = self.create_task(class_name=SaveToS3,
                                             requires=task2,
                                             parameters=param,
                                             s3_url=task_setting.get_s3_file())
                else:
                    task3 = self.create_task(class_name=SaveToHDFS,
                                             requires=task2,
                                             parameters=param,
                                             hdfs_path=task_setting.get_hdfs_file())

                task4 = self.create_task(class_name=DownloadFile,
                                         parameters=param,
                                         file_url=task_setting.get_blacklist_url(),
                                         local_path=task_setting.get_blacklist_path())

                if param["s3"]:
                    task5 = self.create_task(class_name=SaveToS3,
                                             requires=task4,
                                             parameters=param,
                                             s3_url=task_setting.get_s3_blacklist())
                else:
                    task5 = self.create_task(class_name=SaveToHDFS,
                                             requires=task4,
                                             parameters=param,
                                             hdfs_path=task_setting.get_hdfs_blacklist())

                task6 = self.create_task(class_name=SparkSubmit,
                                         requires=[task3, task5],
                                         parameters=param,
                                         app="spark/compute_top_articles.py",
                                         local_path=task_setting.get_local_result_path())

                task7 = self.create_task(class_name=SaveToS3,
                                         requires=task6,
                                         parameters=param,
                                         s3_url=task_setting.get_s3_result_url())

                self.last_tasks.append(task7)

    def get_last_tasks(self):
        """
        Get the last tasks to trigger first
        :return:
        """
        return self.last_tasks
