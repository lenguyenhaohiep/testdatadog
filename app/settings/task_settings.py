from luigi.s3 import S3Client
from luigi.contrib.hdfs.webhdfs_client import WebHdfsClient
from settings.config import Config
import datetime

import configparser


class TaskSettings:
    """
    Provide settings for tasks, which are url, hdfs path, s3url based on parameters
    """
    parser = Config()

    def __init__(self, date, hour):
        self.date = date
        self.hour = hour

    def get_file_url(self):
        file_url = "http://dumps.wikimedia.org/other/pagecounts-all-sites" \
                   "/{year}/{year}-{month}/pagecounts-{year}{month}{day}-{hour}0000.gz"

        date = datetime.datetime.strptime(self.date, "%Y-%m-%d")
        return file_url.format(day=date.strftime("%d"),
                               month=date.strftime("%m"),
                               year=date.strftime("%Y"),
                               hour=self.hour if len(str(self.hour)) == 2 else "0%s" % (self.hour))

    def get_compressed_path(self):
        local_compressed_file = "data/raw/pagecounts_{date}_{hour}.gz"
        return local_compressed_file.format(date=self.date, hour=self.hour)

    def get_uncompressed_path(self):
        local_uncompressed_file = "data/uncompressed/pagecounts_{date}_{hour}.dat"
        return local_uncompressed_file.format(date=self.date, hour=self.hour)

    def get_local_result_path(self):
        local_result_file = "data/result/res_pagecounts_{date}_{hour}.dat"
        return local_result_file.format(date=self.date, hour=self.hour)

    def get_s3_result_url(self):
        s3_result_url = "s3n://{bucket}/test_datadog/result/res_pagecounts_{date}_{hour}.dat"
        return s3_result_url.format(bucket=TaskSettings.parser.get("test_bucket", "bucket"), date=self.date, hour=self.hour)

    def get_s3_file(self):
        s3_result_url = "s3n://{bucket}/test_datadog/raw/pagecounts_{date}_{hour}.dat"
        return s3_result_url.format(bucket=TaskSettings.parser.get("test_bucket", "bucket"), date=self.date, hour=self.hour)

    def get_hdfs_result(self):
        hdfs_result_url = "/test_datadog/result/res_pagecounts_{date}_{hour}.dat"
        return hdfs_result_url.format(date=self.date, hour=self.hour)

    def get_hdfs_file(self):
        hdfs_result_url = "/test_datadog/raw/pagecounts_{date}_{hour}.dat"
        return hdfs_result_url.format(date=self.date, hour=self.hour)

    @staticmethod
    def get_blacklist_url():
        return "https://s3.amazonaws.com/dd-interview-data/data_engineer/wikipedia/blacklist_domains_and_pages"

    @staticmethod
    def get_blacklist_path():
        return "data/raw/blacklist.dat"

    @staticmethod
    def get_s3_blacklist():
        s3_result_url = "s3n://{bucket}/test_datadog/raw/blacklist.dat"
        return s3_result_url.format(bucket=TaskSettings.parser.get("test_bucket", "bucket"))


    @staticmethod
    def get_hdfs_blacklist():
        hdfs_result_url = "/test_datadog/raw/blacklist.dat"
        return hdfs_result_url

    @staticmethod
    def get_s3client():
        s3client = S3Client(aws_access_key_id=TaskSettings.parser.get("s3", "aws_access_key_id"),
                            aws_secret_access_key=TaskSettings.parser.get("s3", "aws_secret_access_key"))
        return s3client

    @staticmethod
    def get_hdfs_client():
        hdfs_client = WebHdfsClient(host=TaskSettings.parser.get("hdfs", "host"),
                                    port=TaskSettings.parser.get("hdfs", "port"),
                                    user=TaskSettings.parser.get("hdfs", "user"))
        return hdfs_client

    @staticmethod
    def get_aws_credentials():
        return TaskSettings.parser.get("s3", "aws_access_key_id"), TaskSettings.parser.get("s3", "aws_secret_access_key")

    @staticmethod
    def get_hdfs_url():
        return "hdfs://{host}:{port}/".format(host=TaskSettings.parser.get("hdfs", "host"),
                                              port=TaskSettings.parser.get("hdfs", "port2"))
