import sys

from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.types import StructType, StringType, StructField
from pyspark.sql.functions import concat
from settings.task_settings import TaskSettings

args = {
    "input": sys.argv[1],
    "blacklist": sys.argv[2],
    "output": sys.argv[3]
}

delimiter = " "
sc = SparkContext()

aws_key, aws_secret = TaskSettings.get_aws_credentials()

sc._jsc.hadoopConfiguration().set("fs.s3n.awsAccessKeyId", aws_key)
sc._jsc.hadoopConfiguration().set("fs.s3n.awsSecretAccessKey", aws_secret)

sqlcontext = SQLContext(sc)

# read data
raw_data = sqlcontext.read.format("com.databricks.spark.csv") \
    .options(header='true',
             inferSchema='true',
             delimiter=delimiter,
             parserLib='univocity',
             quote='\0',
             mode='DROPMALFORMED') \
    .load(args["input"])

# construct blacklist schema
bl_schema = StructType([StructField("domain_code", StringType(), True),
                        StructField("page_title", StringType(), True)])

# read blacklist
blacklist = sqlcontext.read.format("com.databricks.spark.csv") \
    .options(header='false',
             delimiter=delimiter,
             parserLib='univocity',
             quote='\0',
             mode='DROPMALFORMED') \
    .load(args["blacklist"], schema=bl_schema)

# ID = domain_code + Page_title
blacklist_ids = blacklist.withColumn("id", concat(blacklist["domain_code"], blacklist["page_title"])) \
    .select("id")

articles = raw_data.withColumn("id", concat(raw_data["domain_code"], raw_data["page_title"]))

# Register as table
sqlcontext.registerDataFrameAsTable(blacklist_ids, "blacklist")
sqlcontext.registerDataFrameAsTable(articles, "articles")

# Filter article in blacklist
data = sqlcontext.sql(
    """
    select domain_code, page_title, sum(count_views) as pageviews
    from articles left join blacklist on articles.id = blacklist.id
    where blacklist.id is null
    group by domain_code, page_title
    """)

sqlcontext.registerDataFrameAsTable(data, "views")

# Find rank by group (domain, page_title)
ranked_domains = sqlcontext.sql(
    """
    select
        domain_code,
        page_title,
        pageviews,
        row_number() over (partition by domain_code order by pageviews desc) as rank
    from views
    """)

sqlcontext.registerDataFrameAsTable(ranked_domains, "ranked_domains")

# select top 25 for each group
top = sqlcontext.sql(
    """
    select domain_code, page_title, pageviews
    from ranked_domains
    where rank <= 25
    order by domain_code asc, pageviews desc, page_title asc
    """)

top.show()

# Save to HDFS or S3
top.write.format("com.databricks.spark.csv").options(header='false', delimiter=delimiter).save(args["output"])
