import unittest

from tasks.download_file import DownloadFile
from tasks.extract_file import ExtractFile
from tasks.save_to_s3 import SaveToS3
from tasks.save_to_hdfs import SaveToHDFS
from tasks.spark_submit import SparkSubmit
from settings.task_settings import TaskSettings
from settings.pipeline_builder import PipelineBuilder

import luigi
import luigi.mock
import queue
import os


class MockDownloadFile(DownloadFile):
    def run(self):
        # Do nothing, assume that we have well downloaded the zip file
        pass


class MockSparkSubmit(SparkSubmit):
    master = "local[*]"
    executor_memory = '2G'
    total_executor_cores = 2


class MockPipelineBuilder(PipelineBuilder):
    def build(self):
        self.last_tasks = []

        param = self._get_parameters()[0]
        task_setting = TaskSettings(param["date"], param["hour"])

        task1 = self.create_task(class_name=MockDownloadFile,
                                 parameters=param,
                                 file_url=task_setting.get_file_url(),
                                 local_path="test/data/pagecounts_2016-01-01_2.dat.gz")

        task2 = self.create_task(class_name=ExtractFile,
                                 requires=task1,
                                 parameters=param,
                                 local_path="test/data/pagecounts_2016-01-01_2.dat")

        task3 = self.create_task(class_name=SaveToHDFS,
                                 requires=task2,
                                 parameters=param,
                                 hdfs_path="/test/pagecounts_2016-01-01_2.dat")

        task4 = self.create_task(class_name=DownloadFile,
                                 parameters=param,
                                 file_url=task_setting.get_blacklist_url(),
                                 local_path="test/data/blacklist2.dat")

        task5 = self.create_task(class_name=SaveToHDFS,
                                 requires=task4,
                                 parameters=param,
                                 hdfs_path="/test/blacklist.dat")

        task6 = self.create_task(class_name=MockSparkSubmit,
                                 requires=[task3, task5],
                                 parameters=param,
                                 app="spark/compute_top_articles.py",
                                 local_path="test/data/res_pagecounts_2016-01-01_2.dat")

        task7 = self.create_task(class_name=SaveToHDFS,
                                 requires=task6,
                                 parameters=param,
                                 hdfs_path="/test/res_pagecounts_2016-01-01_2.dat")

        self.last_tasks.append(task7)


class TestPipeline(unittest.TestCase):
    def tearDown(self):
        local_paths = ["test/data/pagecounts_2016-01-01_2.dat", "test/data/blacklist2.dat",
                       "test/data/res_pagecounts_2016-01-01_2.dat"]
        hdfs = ["/test/res_pagecounts_2016-01-01_2.dat", "/test/blacklist.dat", "/test/pagecounts_2016-01-01_2.dat"]
        s3 = []
        for local in local_paths:
            try:
                os.remove(local)
            except:
                pass

        for h in hdfs:
            if TaskSettings.get_hdfs_client().exists(h):
                TaskSettings.get_hdfs_client().remove(h, skip_trash=True)

        for s in s3:
            if TaskSettings.get_s3client().exists(s):
                TaskSettings.get_s3client().remove(s)

    def test_pipeline_ok(self):
        # Fake date and hour, we use the small data to test, not the data of the given date and hour
        pipeline_builder = MockPipelineBuilder(date='2016-01-01', hour='2')

        luigi.build(pipeline_builder.get_last_tasks(), local_scheduler=True)

        with open("test/data/expected_2016-01-01_2.dat") as fin:
            expected = fin.read()

        with open("test/data/res_pagecounts_2016-01-01_2.dat") as fin:
            actual = fin.read()

        self.assertEqual(expected.strip(), actual.strip())


class TestPipelineCreation(unittest.TestCase):
    def test_pipeline_creation_ok(self):

        pipeline_builder = PipelineBuilder(date='2015-06-04', hour='10')
        pipeline = pipeline_builder.get_last_tasks()

        actual = []
        q = queue.Queue()

        for t in pipeline:
            q.put(t)

        while q.empty() is False:
            task = q.get()
            actual.append(str(task.__class__.__name__))

            if task.requires() is not None:
                if not isinstance(task.requires(), list):
                    list_dep = [task.requires()]
                else:
                    list_dep = task.requires()

                for t in list_dep:
                    q.put(t)

        expected = ['SaveToS3', 'SparkSubmit', 'SaveToHDFS', 'SaveToHDFS', 'ExtractFile',
                    'DownloadFile', 'DownloadFile']

        self.assertEqual(actual, expected)


if __name__ == "__main__":
    unittest.main()
