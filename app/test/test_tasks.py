import unittest
from unittest import mock
import luigi
import luigi.mock

from tasks.download_file import DownloadFile
from tasks.extract_file import ExtractFile
from tasks.save_to_hdfs import SaveToHDFS
from settings.task_settings import TaskSettings
from tasks.spark_submit import SparkSubmit

import os


class MockSparkSubmit(SparkSubmit):
    master = "local[*]"
    executor_memory = '2G'
    total_executor_cores = 2

    def app_options(self):
        host = TaskSettings.get_hdfs_url()

        hdfs_client = TaskSettings.get_hdfs_client()

        if hdfs_client.exists(self.temp_dir + self.task_id):
            hdfs_client.remove(self.temp_dir + self.task_id, skip_trash=True)

        return [i.path for i in self.input()] + [host + self.temp_dir + self.task_id]


class TestDownloadTask(unittest.TestCase):
    def test_succesful_download(self):
        file_url = "https://s3.amazonaws.com/dd-interview-data/data_engineer/wikipedia/blacklist_domains_and_pages"

        download_task = DownloadFile(file_url=file_url,
                                     local_path="test/data/blacklist.dat")

        luigi.build([download_task], local_scheduler=True)

        self.assertTrue(download_task.output().exists())
        os.remove("test/data/blacklist.dat")

    def test_domain_error(self):
        file_url = "https://s3.amazonaws.com/error_domain"

        download_task = DownloadFile(file_url=file_url,
                                     local_path='test/data/error.dat')

        luigi.build([download_task], local_scheduler=True)

        self.assertFalse(download_task.output().exists())


class TestExtractFile(unittest.TestCase):
    def test_extract_error_zip_file(self):
        def input():
            return luigi.LocalTarget("test/data/error_zipfile.gz")

        extract_file = ExtractFile(local_path="test/fake/path")
        extract_file.input = input

        luigi.build([extract_file], local_scheduler=True)
        self.assertFalse(extract_file.output().exists())

    def test_extract_zip_file_ok(self):
        def input():
            return luigi.LocalTarget("test/data/pagecounts_2016-01-01_2.dat.gz")

        extract_file = ExtractFile(local_path="test/data/pagecounts_2016-01-01_2.dat")
        extract_file.input = input

        luigi.build([extract_file], local_scheduler=True)

        self.assertTrue(extract_file.output().exists())

        os.remove("test/data/pagecounts_2016-01-01_2.dat")


class TestSaveToHDFS(unittest.TestCase):
    def test_save_hdfs_ok(self):
        test_path = "/test/test.file"
        save_to_hdfs = SaveToHDFS(hdfs_path=test_path)

        def input():
            return luigi.LocalTarget("test/data/pagecounts_2016-01-01_2.dat.gz")

        save_to_hdfs.input = input

        luigi.build([save_to_hdfs], local_scheduler=True)
        self.assertTrue(save_to_hdfs.output().exists())

        TaskSettings.get_hdfs_client().remove(save_to_hdfs.output().path, skip_trash=True)


class TestSparkSubmit(unittest.TestCase):
    def test_csv_ok(self):
        def input():
            return [
                luigi.LocalTarget("test/data/data_test_ok_csv.dat"),
                luigi.LocalTarget("test/data/blacklist_test.dat")
            ]

        spark_job = MockSparkSubmit(local_path="test/data/res_ok_csv_test.dat",
                                    app="spark/compute_top_articles.py")
        spark_job.input = input

        luigi.build([spark_job], local_scheduler=True)
        with open("test/data/expected_2016-01-01_2.dat") as fin:
            expected = fin.read()

        with open("test/data/res_ok_csv_test.dat") as fin:
            actual = fin.read()

        self.assertEqual(expected.strip(), actual.strip())

        os.remove("test/data/res_ok_csv_test.dat")

    def test_malformed_csv_file(self):
        def input():
            return [
                luigi.LocalTarget("test/data/data_test_malformed_csv.dat"),
                luigi.LocalTarget("test/data/blacklist_test.dat")
            ]

        spark_job = MockSparkSubmit(local_path="test/data/res_malformed_csv_test.dat",
                                    app="spark/compute_top_articles.py")
        spark_job.input = input

        luigi.build([spark_job], local_scheduler=True)
        with open("test/data/expected_malformed_csv_test.dat") as fin:
            expected = fin.read()

        with open("test/data/res_malformed_csv_test.dat") as fin:
            actual = fin.read()

        self.assertEqual(expected.strip(), actual.strip())

        os.remove("test/data/res_malformed_csv_test.dat")


if __name__ == "__main__":
    unittest.main()
