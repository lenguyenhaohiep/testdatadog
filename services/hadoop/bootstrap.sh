#!/bin/bash

: ${HADOOP_PREFIX:=/usr/local/hadoop}

$HADOOP_PREFIX/etc/hadoop/hadoop-env.sh

rm /tmp/*.pid

# installing libraries if any - (resource urls added comma separated to the ACP system variable)
cd $HADOOP_PREFIX/share/hadoop/common ; for cp in ${ACP//,/ }; do  echo == $cp; curl -LO $cp ; done; cd -

# altering the core-site configuration
sed s/HOSTNAME/$HOSTNAME/ /usr/local/hadoop/etc/hadoop/core-site.xml.template > /usr/local/hadoop/etc/hadoop/core-site.xml

# setting spark defaults
echo spark.yarn.jar hdfs:///spark/spark-assembly-2.0.0-hadoop2.6.0.jar > $SPARK_HOME/conf/spark-defaults.conf
cp $SPARK_HOME/conf/metrics.properties.template $SPARK_HOME/conf/metrics.properties

service sshd start
$HADOOP_PREFIX/sbin/start-dfs.sh
$HADOOP_PREFIX/sbin/start-yarn.sh

#/usr/local/spark-2.0.0-bin-hadoop2.6/bin/spark-class org.apache.spark.deploy.master.Master -h hadoop
./usr/local/spark-2.0.0-bin-hadoop2.6/sbin/start-master.sh
./usr/local/spark-2.0.0-bin-hadoop2.6/sbin/start-slave.sh spark://hadoop:7077

#/usr/local/spark-2.0.0-bin-hadoop2.6/bin/spark-class org.apache.spark.deploy.worker.Worker spark://hadoop:7077 &
#/usr/local/spark-2.0.0-bin-hadoop2.6/bin/spark-class org.apache.spark.deploy.worker.Worker spark://hadoop:7077 &
#/usr/local/spark-2.0.0-bin-hadoop2.6/bin/spark-class org.apache.spark.deploy.worker.Worker spark://hadoop:7077

CMD=${1:-"exit 0"}
if [[ "$CMD" == "-d" ]];
then
	service sshd stop
	/usr/sbin/sshd -D -d
else
	/bin/bash -c "$*"
fi
